# > author：xiaozeng
# > date：2024-10-24

[General]
# > 日志级别
loglevel = notify
# > 当遇到 REJECT 策略时返回错误页
show-error-page-for-reject = true
# > 当节点不支持UDP时，默认fallback 到reject
udp-policy-not-supported-behaviour = reject
# UDP 优先级
udp-priority = false
# > Wi-Fi 访问
allow-wifi-access = false
# > All Hybrid 网络并发
all-hybrid = false
# > IPv6 支持（默认关闭）
ipv6 = true
ipv6-vif = auto
# > 测试超时（秒）
test-timeout = 3
# > Internet 测试 URL
internet-test-url = http://www.baidu.com
# > 代理测速 URL
proxy-test-url = http://www.gstatic.com/generate_204
# > 自定义 GeoIP 数据库
geoip-maxmind-url = https://github.com/Hackl0us/GeoIP2-CN/raw/release/Country.mmdb
# > All Hybrid 网络并发
exclude-simple-hostnames = true
# > 排除简单主机名
dns-server = 223.5.5.5, 119.29.29.29, 2400:da00::6666, 192.18.0.2, system
# > DNS 服务器
hijack-dns = 8.8.8.8:53, 8.8.4.4:53
encrypted-dns-server = https://doh.pub/dns-query, https://dns.alidns.com/dns-query
# DoH 服务器
read-etc-hosts = true
# > 从 /etc/hosts 读取 DNS 记录
http-api-web-dashboard = false
# HTTP-API控制
use-default-policy-if-wifi-not-primary = false
# > 远程控制器
skip-proxy = 127.0.0.1,192.168.0.0/16,10.0.0.0/8,172.16.0.0/12,100.64.0.0/10,localhost,*.local,e.crashlytics.com,captive.apple.com,::ffff:0:0:0:0/1,::ffff:128:0:0:0/1
# 跳过代理
always-real-ip = *.srv.nintendo.net, *.stun.playstation.net, xbox.*.microsoft.com, *.xboxlive.com*.srv.nintendo.net, *.stun.playstation.net, xbox.*.microsoft.com, *.xboxlive.com, *.battlenet.com.cn, *.battlenet.com, *.blzstatic.cn, *.battle.net
# > Always Real IP Hosts
# > 兼容模式(若需要使用Homekit摄像头，可以开启兼容模式，但开启兼容模式会导致一些屏蔽广告的模块失效，请自行取舍)
# compatibility-mode = 5
# > Surge VIF
# tun-excluded-routes = 192.168.0.0/16, 10.0.0.0/8, 172.16.0.0/12
# tun-included-routes = 192.168.1.12/32

[Proxy]
🇭🇰BWG-HK = snell, 45.78.34.245, 11807, psk=cec515d1-c6d1-46a2-bdff-9ade684822e2, version=4, block-quic=on
🇸🇬aws-hy2 = hysteria2, 13.250.41.169, 443, username=387995e6-ad01-4261-be8f-ce16d063c917, password=5201314, skip-cert-verify=true, sni=bing.com
🇺🇸zgo-us = vmess, zgo.520126.xyz, 443, username=cec515d1-c6d1-46a2-bdff-9ade684822e2, skip-cert-verify=true, sni=bing.com, ws=true, ws-path=/ray, ws-headers=Host:"zgo.520126.xyz", vmess-aead=true, tls=true
🇸🇬oracle-sg = vmess, sg.520126.xyz, 443, username=cec515d1-c6d1-46a2-bdff-9ade684822e2, skip-cert-verify=true, sni=bing.com, ws=true, ws-path=/ray, ws-headers=Host:"sg.520126.xyz", vmess-aead=true, tls=true, tfo=true
🇺🇸CC-10.99 = snell, 74.48.24.225, 11807, psk=cec515d1-c6d1-46a2-bdff-9ade684822e2, version=4
🇸🇬oracle = snell, 140.245.61.246, 11807, psk=cec515d1-c6d1-46a2-bdff-9ade684822e2, version=4
🇭🇰BWG-vmess = vmess, hk.520126.xyz, 443, username=cec515d1-c6d1-46a2-bdff-9ade684822e2, skip-cert-verify=true, sni=bing.com, ws=true, ws-path=/ray, ws-headers=Host:"hk.520126.xyz", vmess-aead=true, tls=true, block-quic=on

[Proxy Group]
# ##------------------------------------
# # > 策略组（下面的节点信息需与外部节点对应，若删除了外部节点里的节点，那么在策略组里也要删除。）
Proxy = select, 🇭🇰BWG-HK, 🇸🇬aws-hy2, no-alert=0, hidden=0, include-all-proxies=0, icon-url=https://raw.githubusercontent.com/fmz200/wool_scripts/main/icons/apps/Surge.png, policy-path=https://sub.store/download/collection/%E6%9C%BA%E5%9C%BA%E5%90%88%E9%9B%86, update-interval=0
# > Google策略
Google = select, 🇭🇰BWG-HK, HK, US, Proxy, no-alert=0, hidden=0, include-all-proxies=0, icon-url=https://raw.githubusercontent.com/fmz200/wool_scripts/main/icons/apps/Google.png
# > YouTube策略
YouTube = select, 🇭🇰BWG-HK, HK, US, SG, no-alert=0, hidden=0, include-all-proxies=0, icon-url=https://raw.githubusercontent.com/Koolson/Qure/master/IconSet/Color/YouTube.png
# > Netflix策略
Netflix = select, HK, SG, JP, Proxy, no-alert=0, hidden=0, include-all-proxies=0, icon-url=https://raw.githubusercontent.com/fmz200/wool_scripts/main/icons/lige47/netflix(red).png
# > HBOMax策略
Max = select, HK, US, SG, Proxy, no-alert=0, hidden=0, include-all-proxies=0, icon-url=https://raw.githubusercontent.com/Koolson/Qure/master/IconSet/Color/HBO_Max.png
# > Speedtest策略
Speedtest = select, 🇭🇰BWG-HK, 🇸🇬oracle, 🇸🇬oracle-sg, 🇸🇬aws-hy2, 🇺🇸zgo-us, 🇺🇸CC-10.99, no-alert=0, hidden=0, include-all-proxies=0, icon-url=https://raw.githubusercontent.com/Koolson/Qure/master/IconSet/Color/Speedtest.png
# > Disney+策略
Disney+ = select, HK, US, SG, Proxy, no-alert=0, hidden=0, include-all-proxies=0, icon-url=https://raw.githubusercontent.com/Orz-3/mini/master/Color/DisneyPlus.png
# > Spotify策略
Spotify = select, 🇭🇰BWG-HK, HK, US, DIRECT, no-alert=0, hidden=0, include-all-proxies=0, icon-url=https://raw.githubusercontent.com/fmz200/wool_scripts/main/icons/apps/spotify.png
# > TikTok策略
TikTok = select, JP, US, SG, Proxy, no-alert=0, hidden=0, include-all-proxies=0, icon-url=https://raw.githubusercontent.com/Koolson/Qure/master/IconSet/Color/TikTok_2.png

# ##------------------------------------
# # > 地区分组策略
# > 香港
HK = fallback, policy-path=https://sub.store/download/collection/%E6%9C%BA%E5%9C%BA%E5%90%88%E9%9B%86, update-interval=0, policy-regex-filter=(🇭🇰)|(港)|(Hong)|(HK), no-alert=0, hidden=0, include-all-proxies=0, icon-url=https://raw.githubusercontent.com/Koolson/Qure/master/IconSet/Color/Hong_Kong.png
# > 美国
US = fallback, policy-path=https://sub.store/download/collection/%E6%9C%BA%E5%9C%BA%E5%90%88%E9%9B%86, update-interval=0, policy-regex-filter=(🇺🇸)|(美)|(States)|(US), no-alert=0, hidden=0, include-all-proxies=0, icon-url=https://raw.githubusercontent.com/Koolson/Qure/master/IconSet/Color/United_States.png
# > 新加坡
SG = fallback, policy-path=https://sub.store/download/collection/%E6%9C%BA%E5%9C%BA%E5%90%88%E9%9B%86, update-interval=0, policy-regex-filter=(🇸🇬)|(新家坡)|(Singapore)|(SG), no-alert=0, hidden=0, include-all-proxies=0, icon-url=https://raw.githubusercontent.com/Koolson/Qure/master/IconSet/Color/Singapore.png
# > 日本
JP = fallback, policy-path=https://sub.store/download/collection/%E6%9C%BA%E5%9C%BA%E5%90%88%E9%9B%86, update-interval=0, policy-regex-filter=(🇯🇵)|(日本)|(Japan)|(JP), no-alert=0, hidden=0, include-all-proxies=0, icon-url=https://raw.githubusercontent.com/Koolson/Qure/master/IconSet/Color/Japan.png

# ##------------------------------------
# # > 机场订阅
# > M78star
M78 = select, policy-path=https://sub.7878787878.top/api/v1/client/subscribe?token=5718185d3d4f9b14af9db56740392dea, update-interval=0, no-alert=0, hidden=0, include-all-proxies=0, icon-url=https://gitlab.com/xiaozeng/icon/-/raw/main/m78star.png
# > Academy City Airport
ACA = select, policy-path=https://dadada.acaisbest.com/api/v1/client/subscribe?token=c88dcb166c62f908d6773c0c19d7487e, update-interval=0, no-alert=0, hidden=0, include-all-proxies=0, icon-url=https://raw.githubusercontent.com/Jasonaax/Surge/main/Icon/Airport/ACA.png

# ##------------------------------------
# > Tidal策略
Tidal = select, 🇭🇰BWG-HK, HK, US, no-alert=0, hidden=1, include-all-proxies=0, icon-url=https://raw.githubusercontent.com/Koolson/Qure/master/IconSet/Color/TIDAL_2.png
# > ChatGPT策略
ChatGPT = select, US, JP, SG, Proxy, no-alert=0, hidden=1, include-all-proxies=0, icon-url=https://raw.githubusercontent.com/chxm1023/Script_X/main/icon/ChatGPT/ChatGPT4.png
# > Apple策略
Apple = select, DIRECT, HK, US, SG, hidden=1 hidden=1, include-all-proxies=0, icon-url=https://gitlab.com/xiaozeng/icon/-/raw/main/Apple.png

[Rule]
# Apple规则
RULE-SET,https://raw.githubusercontent.com/blackmatrix7/ios_rule_script/master/rule/Surge/Apple/Apple.list,Apple
# Bing规则
RULE-SET,https://raw.githubusercontent.com/blackmatrix7/ios_rule_script/master/rule/Surge/Bing/Bing.list,US
# ChatGPT规则
RULE-SET,https://raw.githubusercontent.com/blackmatrix7/ios_rule_script/master/rule/Surge/OpenAI/OpenAI.list,ChatGPT
# YouTube规则
RULE-SET,https://raw.githubusercontent.com/blackmatrix7/ios_rule_script/master/rule/Surge/YouTube/YouTube.list,YouTube
# Netflix规则
RULE-SET,https://raw.githubusercontent.com/blackmatrix7/ios_rule_script/master/rule/Surge/Netflix/Netflix.list,Netflix
# HBOMax规则
RULE-SET,https://raw.githubusercontent.com/blackmatrix7/ios_rule_script/master/rule/Surge/HBO/HBO.list,Max
# Disney+规则
RULE-SET,https://raw.githubusercontent.com/blackmatrix7/ios_rule_script/master/rule/Surge/Disney/Disney.list,Disney+
# Tidal规则
RULE-SET,https://raw.githubusercontent.com/blackmatrix7/ios_rule_script/master/rule/Surge/TIDAL/TIDAL.list,Tidal
# Spotify规则
RULE-SET,https://raw.githubusercontent.com/blackmatrix7/ios_rule_script/master/rule/Surge/Spotify/Spotify.list,Spotify
# TikTok规则
RULE-SET,https://raw.githubusercontent.com/blackmatrix7/ios_rule_script/master/rule/Surge/TikTok/TikTok.list,TikTok
# Google规则
RULE-SET,https://raw.githubusercontent.com/blackmatrix7/ios_rule_script/master/rule/Surge/Google/Google.list,Google
# Sub-Store规则
RULE-SET,https://raw.githubusercontent.com/getsomecat/GetSomeCats/Surge/rule/substore.list,🇭🇰BWG-HK
# Proxy规则
RULE-SET,https://raw.githubusercontent.com/blackmatrix7/ios_rule_script/master/rule/Surge/Proxy/Proxy.list,Proxy
# Speedtest规则
RULE-SET,https://raw.githubusercontent.com/blackmatrix7/ios_rule_script/master/rule/Surge/Speedtest/Speedtest.list,Speedtest
# Mainland规则
RULE-SET,https://raw.githubusercontent.com/blackmatrix7/ios_rule_script/master/rule/Surge/ChinaMax/ChinaMax_All.list,DIRECT
# YuhengVIP规则
RULE-SET,https://gist.githubusercontent.com/Yuheng0101/a7a432754e79bf2f653e2fb6ec1aa8ea/raw/DIRECT.list,DIRECT
# 局域网地址
RULE-SET,https://raw.githubusercontent.com/blackmatrix7/ios_rule_script/master/rule/Surge/Lan/Lan.list,DIRECT
# ChinaASN规则
RULE-SET,https://raw.githubusercontent.com/DecoAri/Rules/Master/Surge/CHINA_ASN.list,DIRECT


# GEOIP
GEOIP,CN,DIRECT,no-resolve

# DNS 查询失败走Final规则
FINAL,🇭🇰BWG-HK,dns-failed

[MITM]
# > 主机名
hostname = kaka-tech.com
# > ca证书
ca-passphrase = 5A0BD05D
ca-p12 = MIIKPAIBAzCCCgYGCSqGSIb3DQEHAaCCCfcEggnzMIIJ7zCCBF8GCSqGSIb3DQEHBqCCBFAwggRMAgEAMIIERQYJKoZIhvcNAQcBMBwGCiqGSIb3DQEMAQYwDgQIoiYRs6ESddsCAggAgIIEGPtMiCmRDy5VLowcnTxUeBkbsoNPAHpUf+xv73WtTMOTvgSiucAKGdChfR3z5pHjCOMQoh4/OSR6ILDGQ5HSW8SGWPqotISkrmAYEyFV3mFi9uAXN94SGvGUgwgfS9k4ZvDUKVqpV6d4sH4QAspNNxtI5cmDd2VgKNR2SGoVXXErC8PtRidr/Ij0imQxhpiaJVDH+qRatMsm8TtMT3wLuE7J7fGnhmGGTy5Psz4SSa+J3zARUiZu7tV2V4rZpWmw1V2IlK8v9HRpZDzpjxMUDAD3iRMPso8UEc7pN1eFWtunfpL6vFfq36fz2uET6juEyyI5NaCCx9A1UFzXpkGSyLkWJCK0IAJH5gsD3qfj4UJtAlC80+6ne74ZVWH5hAPcpV1RV08PdLeozggCjpan73MzjIDPcnBqBtsPJsQo5dC/drA4yEAFh41OJODhm1SPZbDy8QOtpXAZ63wkZ5IF7rodx9b8FVLz4c8npUjBbk5HWl1Fbzrx+eDJglgaphDsd1+Nq/MfFBuhx2dgvsIvzQi30u1xhxI10BwRz9oaE4Ph2PXnI/khF/pDdzygQ+TIqz9JsTVVL0nNeWklR+7KLpFxW9S+4MqSjnmPqgwjB13iAMhS3wTs8QXeW23LrnAOuAhzoesAKYDnSoY3CDxSU5u7e+a0gKRN9k9ekSM/CD6nFRTwnGc3Nksn33O6D5eHVyOP57mA6qSa1cBS3WU2Wo+rCYP2ChHzKwGMqOPlT30eIiUvqFptoNKUTxB4H14iwCEoH/kkmafY7QstVAWXd8CrOAf//qidty2XQYCTEf+1dvTNzI4HTdwaeBIIGtqoAIZIl1hQPYha5EPfk5l03T+W8TkRL0EoNea9wWy8e2Fxm1uKaHY8UWbPzBISt5VMxufO0KEDctYwFVPW7a9LFURC3/L0aTKsu/5AoaCBI30tEBWNhxmpOhT3VloHqgOac0ADj3D6/Xa/srvifww8pfAul1sRyybp/sv9U3AQ8Fb2JBP0ESZ4gn2o3Pz7PCbFTz/zmMY0kDs1YtHKncwvdzXsrtcUelqKis6Y4aiomkTwPLUkgwSELaYtvkpXL+iMHHLtXzfRV/ZKtTDzvknUpl1L9/JyjoyM0rGqpC2EdFTRSIttN8nFK9muNGrebgvnicdctxCjUcvB1FEH9YRNNXBjSF61+MBfwwY6YL7p+s4uf3ulJymzNOIQ6W8YI0A7UIYVc3ehdcu6QCoLmnFS257mjzw/qUveKVLTM7RjjNhECy5IMHWXvrnE+4rNb47eqw3zKmB8agoUaocDTdXBTMrC/fTbdS8l7NzUGANvBfvQ9/BCUtNvSV6FeN9eCbGxSGqn3uzpidFpJr/nzhWc4WPGturqiE+eeva7CCip6c4vu/VNhBYXj5gwggWIBgkqhkiG9w0BBwGgggV5BIIFdTCCBXEwggVtBgsqhkiG9w0BDAoBAqCCBO4wggTqMBwGCiqGSIb3DQEMAQMwDgQIBQdZmYP0UEQCAggABIIEyBh41dTpuc0hRsLJDE5GqEblfEL/fFCL3tDElRzda3Yy3Lj5phZ78229+OCu+uj2oPPmS72u0o4FIMeOtX9/ITpUKZYQ0zcxhhd6ZQb1TvS6v6+BKRrhyEsNLVZ5HqB24b7idGuV3IMNq+WP1v/1DOzbCLobMybqeO9KkaOaGBU2wMpkAd7VbispUFW3kryaMrn9h5JVB+IPLfVhlBiuk3nb82hjjrl2KBNju3vVO694IhaNIJeSFsX3JeQRM11m3l7dvi9JJ9SIRRtSsXHDsGBTiKBmPn8JQFVneUzy6U3qe7FZBLD6UVrh1UeIRpmdBDbca0sWEyBrabmvorlPoDsE2F9a42wA6uOO0tXaNQ9qAVo/Tag/FR5QM2DvC7WYnaJA6NBFt9AmG7h+5bxk9FBIKtVDXyEq6HiiMjSiyw3K33/fetAUl5WjqSog+Qodk0egfChQF3ISg8XRAgdCkav1zUXq+6IxgACceM+zthbWrAP3ed8F3Vxt9WE7Td2iSGlbvsyZdtEfEmDw/b1llVwrBrKNTKH+nIOR3NWQzOudkEOfg770ZNG5IKpS6fan4M322eoSk+fFK4bgnjxaVFyXRNVsev9M+w0tkNE0PNNYncmwp7lwLfjMYbElJmr484uqKtiJSa8et9wX9yV0RuyFyXlvOLakDCH9q4NSMfQqZoaoc6g+auZ3FhprAmCWuKYyxF1Tiq6lREytoa8wRe06BC45fzBFlv7F9MPjjelY00WqKRXGrAhbgckGuL37/O2m5J2uNHBaVXfP2B9tp0FK3n/vA/EwW3Eg16pGVbkRxpVvfjK3toEpTl5S5xsfVSZEpA5zCpxL1fVmVA6x5xOGIF7QAhnXHZKav62gSMqSaLQYqb3hitToNiSmb9ca6BrPQicd4VPgqv4L9TNfA6KfdWzr7pVzX6aXtnSToQBORXYA73xvb/e4AWOagQc43a6p1qGL3GpxEX+has8rnNDdv1+Khj4UnVRAxusg6rDHfjAIawJumr9L+q4m2OiKj7bpStq60Hy4FMo4sNgJkK3xemExpV8rEUNgmr4osmC3B3VWLd7gC51HoWjMnjtd73lTkYjmpEhxZ5OS5QKDI8Zcz3v7LVtYv1fFDdeSNWa+kb17W1rXKiq6fsDJU4xAU57R/fbauehKJAY8Q6M9COi2wbrmB2wjkc1D2il70M3B7Fys5gePdpjoAIo9PGhH1Xv+qqpZPe6lzjjrzpWsVUYt0SlH9rblUdKivXN4eh3sR3cxL5dOc/zdrEVZHuC7S4xM7e2FEgndN+xE4qhaZVo1Cw4B7tnE7qgYmrfTMg85UXYMSqNomW9yJ7PrAU0ryXwq6KBzczYHj2E/GJxpJW+nFMfFcE5AphbYanL8mqlilWBCHaNf9NXFN6e1r/Ek4jHfmOyJUrSl34ZS6R1/xpRUKsEenNgx+K6QMsG1zTPEQq/BSKBF1VelnI3mUkm3i+Vq+oC2X/HJ2tjty0px8LL5Yf82NxW84LFrPrVxDi7C+vdj5slC4Tdxrwk4hpkNSoGF8mCl42yGEZtMlZbDnQkr4gNMdkqIX5DPcqtUme7BcyOviGitVQ9QUes8LfDRtqTUgFhL6QkwK7LepbI5DR1w1kbnIR7pBTFsMCMGCSqGSIb3DQEJFTEWBBTckb8PBnFpLb6OhxbOtMhxAqZRNTBFBgkqhkiG9w0BCRQxOB42AFMAdQByAGcAZQAgAEcAZQBuAGUAcgBhAHQAZQBkACAAQwBBACAANQBBADAAQgBEADAANQBEMC0wITAJBgUrDgMCGgUABBQvZPvf7PqMv1GoenBRr7SxAtM6cgQIBlksqwcBFiA=

