# xiaozeng自用配置
# Date: 2024.05.10
# Author: xiaozeng

[General]
## 
# > GEOIP
geoip-url = https://github.com/Hackl0us/GeoIP2-CN/raw/release/Country.mmdb

# > 资源解析器
resource-parser = resource-parser = https://raw.githubusercontent.com/Peng-YM/Sub-Store/master/backend/dist/sub-store-parser.loon.min.js
resource-parser = https://gitlab.com/sub-store/Sub-Store/-/releases/permalink/latest/downloads/sub-store-parser.loon.min.js

# > IPv6开关
ipv6 = true

# > 跳过某个域名或IP段
skip-proxy = 192.168.0.0/16,10.0.0.0/8,172.16.0.0/12,localhost,*.local,e.crashlynatics.com
bypass-tun = 10.0.0.0/8,100.64.0.0/10,127.0.0.0/8,169.254.0.0/16,172.16.0.0/12,192.0.0.0/24,192.0.2.0/24,192.88.99.0/24,192.168.0.0/16,198.51.100.0/24,203.0.113.0/24,224.0.0.0/4,255.255.255.255/32

# > DNS服务器
dns-server = system,119.29.29.29,114.114.114.114,223.5.5.5

# > Wi-Fi访问
allow-wifi-access = true
wifi-access-http-port = 7222
wifi-access-socks5-port = 7221

# > 代理测速URL
proxy-test-url = http://cp.cloudflare.com/generate_204
internet-test-url = http://wifi.vivo.com.cn/generate_204

# > 测速超时(s)
test-timeout = 3
interface-mode = auto

# > 
sni-sniffing = true
fast-switch = false
disconnect-on-policy-change = false
switch-node-after-failure-times = 3


[Proxy]
# > 自建节点
🇭🇰BWG-HK = vmess,hk.520126.xyz,443,auto,"a0defb12-2291-44b5-9fd9-5b41a805b58c",transport=ws,alterId=0,path=/ray,host=hk.520126.xyz,over-tls=true,sni=bing.com,skip-cert-verify=true

[Remote Proxy]
## > 机场订阅
# > M78star
M78star = https://sub.7878787878.top/api/v1/client/subscribe?token=5718185d3d4f9b14af9db56740392dea,parser-enabled = true,udp=true,fast-open=false,vmess-aead=true,skip-cert-verify=default,enabled=true,flexible-sni=true,img-url=https://raw.githubusercontent.com/xioazeng/icon/master/m78star_2.png

[Proxy Chain]

[Proxy Group]
### > 策略组
## > 国外流媒体策略
# > Google策略
Google = select,BWG-HK,US,SG,HK,url = http://cp.cloudflare.com/generate_204,img-url = https://raw.githubusercontent.com/fmz200/wool_scripts/main/icons/lige47/google(1).png

# > YouTube策略
YouTube = select,BWG-HK,US,SG,HK,url = http://cp.cloudflare.com/generate_204,img-url = https://github.com/shindgewongxj/WHATSINStash/raw/main/icon/youtube.png

# > Netflix策略
Netflix = select,US,SG,HK,Available,url = http://cp.cloudflare.com/generate_204,img-url = https://github.com/shindgewongxj/WHATSINStash/raw/main/icon/netflix.png

# > Disney策略
Disney+ = select,US,SG,HK,Available,url = http://cp.cloudflare.com/generate_204,img-url = https://raw.githubusercontent.com/Orz-3/mini/master/Color/DisneyPlus.png

# > Spotify
Spotify = select,US,SG,HK,TW,DIRECT,Available,url = http://cp.cloudflare.com/generate_204,img-url = https://raw.githubusercontent.com/lige47/QuanX-icon-rule/main/icon/spotify(green).png

# > Tidal策略
Tidal = select,US,HK,url = http://cp.cloudflare.com/generate_204,img-url = https://raw.githubusercontent.com/xioazeng/icon/master/tidal.png

# > TikTok策略
TikTok = select,US,SG,JP,TW,url = http://cp.cloudflare.com/generate_204,img-url = https://raw.githubusercontent.com/chxm1023/Script_X/main/icon/TikTok.png

# > 全局策略
Available = select,HK_Filter,TW_Filter,JP_Filter,KR_Filter,US_Filter,SG_Filter,url = http://cp.cloudflare.com/generate_204,img-url = https://raw.githubusercontent.com/Koolson/Qure/master/IconSet/Color/Available.png

## > 地区策略
# > 美国
US = select,US_Filter,url = http://cp.cloudflare.com/generate_204,img-url = https://raw.githubusercontent.com/lige47/QuanX-icon-rule/main/icon/US(1).png

# > 新加坡
SG = select,SG_Filter,url = http://cp.cloudflare.com/generate_204,img-url = https://raw.githubusercontent.com/Koolson/Qure/master/IconSet/Color/Singapore.png

# > 香港
HK = select,HK_Filter,url = http://cp.cloudflare.com/generate_204,img-url = https://raw.githubusercontent.com/Koolson/Qure/master/IconSet/Color/Hong_Kong.png

# > 台湾
TW = select,TW_Filter,url = http://cp.cloudflare.com/generate_204,img-url = https://raw.githubusercontent.com/Koolson/Qure/master/IconSet/Color/Taiwan.png

# > 日本
JP = select,JP_Filter,url = http://cp.cloudflare.com/generate_204,img-url = https://raw.githubusercontent.com/Koolson/Qure/master/IconSet/Color/Japan.png

## > 国外app策略
# > Apple策略
Apple = select,DIRECT,US,SG,Available,url = http://cp.cloudflare.com/generate_204,img-url = https://github.com/shindgewongxj/WHATSINStash/raw/main/icon/applewwdc23.png

# > ChatGPT策略
ChatGPT = select,Available,US,SG,url = http://cp.cloudflare.com/generate_204,img-url = https://raw.githubusercontent.com/chxm1023/Script_X/main/icon/ChatGPT/ChatGPT4.png

# > 兜底策略
Final = select,DIRECT,US,SG,HK,TW,JP,url = http://cp.cloudflare.com/generate_204,img-url = https://raw.githubusercontent.com/Koolson/Qure/master/IconSet/Color/Final.png

[Remote Filter]
HK_Filter = NameRegex, FilterKey = "(?i)(港|HK|Hong)"
TW_Filter = NameRegex, FilterKey = "(?i)(台|TW|Tai)"
JP_Filter = NameRegex, FilterKey = "(?i)(日本|川日|东京|大阪|泉日|埼玉|沪日|深日|JP|Japan)"
US_Filter = NameRegex, FilterKey = "(?i)(美|波特兰|达拉斯|俄勒冈|凤凰城|费利蒙|硅谷|拉斯维加斯|洛杉矶|圣何塞|圣克拉拉|西雅图|芝加哥|US|United States)"
SG_Filter = NameRegex, FilterKey = "(?i)(新加坡|坡|狮城|SG|Singapore)"

[Rule]
#Type:DOMAIN-SUFFIX,DOMAIN,DOMAIN-KEYWORD,USER-AGENT,URL-REGEX,IP-CIDR
#Strategy:DIRECT,PROXY,REJECT
#Options:no-resolve(only for IP-CIDR,IP-CIDR2,GEOIP,IP-ASN)

GEOIP,cn,DIRECT
FINAL,🇭🇰BWG-HK

[Remote Rule]
### > 远程分流规则订阅
## > 将网络请求进行分流，网络请求的走向(是否能成功访问)由匹配到的策略组决定
# > Disney规则
https://raw.githubusercontent.com/blackmatrix7/ios_rule_script/master/rule/Loon/Disney/Disney.list, policy=Disney+, tag=Disney+, enabled=true

# > Netflix规则
https://raw.githubusercontent.com/blackmatrix7/ios_rule_script/master/rule/Loon/Netflix/Netflix.list, policy=Netflix, tag=Netflix, enabled=true

# > TikTok规则
https://gitlab.com/lodepuly/vpn_tool/-/raw/master/Tool/Loon/Rule/TikTok.list, policy=TikTok, tag=TikTok, enabled=true

# > Spotify规则
https://raw.githubusercontent.com/blackmatrix7/ios_rule_script/master/rule/Surge/Spotify/Spotify.list, policy=Spotify, tag=Spotify, enabled=true

# > Tidal规则
https://raw.githubusercontent.com/blackmatrix7/ios_rule_script/master/rule/Loon/TIDAL/TIDAL.list, policy=Tidal, tag=TIDAL, enabled=true

# > Google规则
https://raw.githubusercontent.com/blackmatrix7/ios_rule_script/master/rule/Loon/Google/Google.list, policy=Google, tag=Google, enabled=true

# > YouTube规则
https://gitlab.com/lodepuly/proxy_tool_resources/-/raw/master/rule/Loon/YouTube/YouTube.list, policy=YouTube, tag=YouTube, enabled=true

# > AI规则
https://gitlab.com/lodepuly/vpn_tool/-/raw/master/Tool/Loon/Rule/AI.list, policy=ChatGPT, tag=OpenAI, enabled=true

# > Apple规则
https://raw.githubusercontent.com/blackmatrix7/ios_rule_script/master/rule/Loon/Apple/Apple.list, policy=Apple, tag=Apple, enabled=true

# > China_ASN规则
https://raw.githubusercontent.com/blackmatrix7/ios_rule_script/master/rule/Loon/ChinaASN/ChinaASN.list, policy=DIRECT, tag=China_ASN, enabled=true

# > China规则
https://raw.githubusercontent.com/blackmatrix7/ios_rule_script/master/rule/Loon/China/China.list, policy=DIRECT, tag=Mainland, enabled=true
https://raw.githubusercontent.com/blackmatrix7/ios_rule_script/master/rule/Surge/ChinaMax/ChinaMax_All.list, policy=DIRECT, tag=Mainland_Max, enabled=true
https://raw.githubusercontent.com/blackmatrix7/ios_rule_script/master/rule/Loon/China/China_Domain.list, policy=DIRECT, tag=Mainland_Resolve, enabled=true

# > 直连规则
https://gist.githubusercontent.com/Cyb3rF1r3/c4fecb21093aa6e65c8f8e47183fd4a9/raw/DIRECT.list, policy=DIRECT, tag=Yuheng VIP, enabled=true

[Rewrite]

[Script]


[Remote Script]
## > 远程脚本订阅
# > ChatGPT解锁查询
https://gist.githubusercontent.com/xioazeng/30c988c696fd976a0de9b954418c83af/raw/d756eeaf603f30f6dba464858c93fc4808f30994/ChatGPT.js, tag=ChatGPT解锁查询, enabled=true

# > Spotify注册检测
https://gist.githubusercontent.com/xioazeng/30c988c696fd976a0de9b954418c83af/raw/42ddd0bbcc8d43668465890212302dff12d37218/Spotify.js, tag=🎵Spotify注册检测, enabled=true
[Plugin]
## > 插件订阅
# > Yuheng VIP
http://script.hub/file/_start_/https://gist.githubusercontent.com/Cyb3rF1r3/5c000e56caec6089476fe5e0b651fe3b/raw/sfw.conf/_end_/sfw.plugin?type=qx-rewrite&target=loon-plugin&del=true, tag=SFW, enabled=true
http://script.hub/file/_start_/https://gist.githubusercontent.com/Cyb3rF1r3/5c000e56caec6089476fe5e0b651fe3b/raw/free.conf/_end_/free.plugin?type=qx-rewrite&target=loon-plugin&del=true, tag=Free, img-url=https://gitlab.com/xiaozeng/icon/-/raw/main/Microsoft.png, enabled=true
http://script.hub/file/_start_/https://gist.githubusercontent.com/Cyb3rF1r3/5c000e56caec6089476fe5e0b651fe3b/raw/nsfw.conf/_end_/nsfw.plugin?type=qx-rewrite&target=loon-plugin&del=true, tag=NSFW, img-url=https://gitlab.com/xiaozeng/icon/-/raw/main/tangxin.png, enabled=true

# > 本地数据管理工具🔧
https://gitlab.com/xiaozeng/plugin/-/raw/main/boxjs.plugin, policy=BWG-HK, enabled=true
https://gitlab.com/xiaozeng/plugin/-/raw/main/Sub-Store.plugin, policy=BWG-HK, enabled=true
https://gitlab.com/xiaozeng/plugin/-/raw/main/Script-Hub.plugin, policy=BWG-HK, enabled=true

# > 节点查询工具🔧
https://gitlab.com/xiaozeng/plugin/-/raw/main/Nodetool.plugin, enabled=true
https://raw.githubusercontent.com/deezertidal/private/master/plugallery/GeoLocation.plugin, tag=GeoLocation, enabled=true
https://gitlab.com/lodepuly/vpn_tool/-/raw/master/Tool/Loon/Plugin/Prevent_DNS_Leaks.plugin, policy=BWG-HK, tag=DNS防泄露, enabled=true
https://gitlab.com/xiaozeng/plugin/-/raw/main/Net_Speed.plugin, enabled=true
https://gitlab.com/xiaozeng/plugin/-/raw/main/net-lsp-x.plugin, enabled=true
https://gitlab.com/xiaozeng/plugin/-/raw/main/LoonGallery.plugin, policy=BWG-HK, enabled=true

# > APP增强工具🔧
https://gitlab.com/xiaozeng/plugin/-/raw/main/Google.plugin, enabled=true
https://gitlab.com/xiaozeng/plugin/-/raw/main/Spotify_lyrics_translation.plugin, enabled=false
https://gitlab.com/xiaozeng/plugin/-/raw/main/DisneyRating.plugin, enabled=true
https://raw.githubusercontent.com/DualSubs/Spotify/main/modules/DualSubs.Spotify.plugin, enabled=true
https://raw.githubusercontent.com/DualSubs/Netflix/main/modules/DualSubs.Netflix.plugin, enabled=false
https://gitlab.com/lodepuly/vpn_tool/-/raw/master/Tool/Loon/Plugin/TikTok_redirect.plugin, policy=JP, enabled=true
https://raw.githubusercontent.com/VirgilClyne/iRingo/main/plugin/TV.plugin, enabled=true

[Mitm]
hostname = 

ca-p12 = MIIJRQIBAzCCCQ8GCSqGSIb3DQEHAaCCCQAEggj8MIII+DCCA68GCSqGSIb3DQEHBqCCA6AwggOcAgEAMIIDlQYJKoZIhvcNAQcBMBwGCiqGSIb3DQEMAQYwDgQIPMl5G/WrhG0CAggAgIIDaDiBFiaqzVB848b7VhKYUR1UZY//m0rEPfp0PTnGBAjFXX3tQglu0pY5ObrgtdGWgnYiEN1g+JmxH45FD/6RUaUSJBOZV57JL2fuacT+UNbVktxuq3S0N72LrdpOz7omafY8djX91xyXx/f1TJviZN/kmASKb1ljf7HFT7SRsl752FYDByFrQEYn7D6hWyzTVvdK3S8pObYhZUgehXzGVmc2CugFCRioGN+dtl/36JeEhhJFlk2xe4SCLfSa2vbjgb4gs+ZOntjCcoeUWWOtQyxnOHq8LLpC3p+dzb/tZzyiPBuRTGSFH0XIP4A6HIBgLRH7QERMN+QueEMPaYoEARl97KQKUzu6ZT4qsKx5KDw7K0DsDL3eRvIfTkJsWwjkrlLKsMBFzJlQbLkU73G/V7LNnqmljMaWcKVLgld9pOCCSPEBy71KyJOF+NA/RiUFiZDx9Bfv+pZQKmNi8j3baXcLXRX6/ndpMqwkN1PbPyfAQp/+AI9TsrBWLJgadWW57HtoqaTXSol8QRj2cgMMHtkEMS5xH2P4cpjwsHk2C3eIP4ahZqstwlvzI96pTCvAxChgmr9x4dG5UR505jeq8G5YVLEHzcnknyOUTHyv5PQo0u/xjJE8o5pfgeHpksO7hwdZWWdo5RLgJ3+00jRhUxCBTAEwu/m5ZT8p1AMB7CYkvpXbkgUDonfd8Zi+AHwpWfxYPX1HriKR/NNI+00kHYlgOCdTmAviwF9ivR4Gd1kUAnkbh5ZzHC7hBXnh50JF+qvEBgEyvbH1boSAYOvDz6INV/utJSo00VlEUGxVKEKSCMPgJJWQTsUaHOJsbv/8wEfL5zM7cuGIMpMKZITBf2ixm97jjXSSJlAhGNm1OboKFL88JYac5bkCXq4HQbS1I7c67A5HSAbyafkS7Sxac1JC6h1DM9xJVIrfEjlNnvHg/9PbDrlbEv5TiOVGnDDQLdvxdchM3Okarzx2QUP+q0uADWG45R37vgGFMR6mLaQ22udFUcsr2/tXIm8FH/EShFV/L7QYW2HgTcfxw0ivGE52yJB6nwMnocCSv0ipkBC1ffAEMYP9uYxEDI4NJnmRms+B/spxxzGj/IAWKBgyRlzDS8T40tFhnTwaUB/tKqkGhe/zFqu4LBF44NidAKNSAFgEyrVIkZ8/MIIFQQYJKoZIhvcNAQcBoIIFMgSCBS4wggUqMIIFJgYLKoZIhvcNAQwKAQKgggTuMIIE6jAcBgoqhkiG9w0BDAEDMA4ECH8J/2msjJnuAgIIAASCBMh0simskL0Iio0mWHFuf82rrnSgEz8PWx3lKQw7itFv3cQSE4IvD3K+ThkAlF+UTJEC4T4sZiJ4qvJHDMKl9IBr9+hpdR+Fwf1VFwDdy4ZsyzcS6j+QJKnJk4JqcpoZUO/7KehZfKFft5lNIPM0ZAixqSTdeb2szStKTYqK3Y3cpiXqFDfVe33G5GRUVOcUkxAyODFghw88yf/ii5c0u1reahZ7WzYFm9fMq3a4/bjTCE8/b71+zgdnkjEK6GjOHc9s5lcu01y/cHzg6Plf+q+sBdCRcQyAMblGKD+/dKypRu5tMPx4e+lsDTS2XJMHjJVIKLuFHO3GTAPds5ljGlt1BhlaEoGr4ux2XrU+/ZwZQVttEaDp+sN9Hg6f7KEw+peeooOsMM/qG9oFD/qj7aybfiZiFye0/28gVAjRqWyKW9MT3sxhLNbW69IeKpyI+2GYvwGT00+JXZjUxHmnAyjfHb+BxCfir9O27sBvoQLAkuLTsDj93fs6v8SANGTdkQuVSkTOhBENH7emv7ltu+PXxPrbpeTRIkTQtb4sOtsEBRSjihrB67Irrae0tp+cGKRgWaZbn+5S9WJLCf7KmhbDXNAfhuslrfsI209se2k+bJF5qTbFu7p3CZ/MbTcETChpPT7dd8ArNf1dtzPEyitrsHKHyOuRnzkyNeESeN62ze1sPW4frW0vIjaeh1RlHEOXV37Y27eX8+Z380JKX8/vxINE8ST3WVlNMhjaKuwQeEuihZR2cOy+RqCkFg14u0MmdUUGRRpD4XoRbysbpyNAv5RAYDgcQA6IRDB7r7sb7TtbbldawVWgRp1pfBKVsU8BeydffDuDbBoWxNYAxIJdOovpKAXe/AEUBiEKYYq5lieBtfliusMWCNep4N7dqz4GpnWKPQkTQ3lZXlMzeYx2yN8KeshchHqH4DECmN5tKLIBxvtqHbz4An36aww3zd2qQQIHNs2gNUsBFWS0LJIFUjheotqLJ9jXziwLlo/dtkrLmwtp8+eLBo4ZpgnpNUGRMbpAobQ3IHURQ71M7h0aO8P30dO8C1G7/4PePmGbXr1IrkWtUsRbjEal7QozcAUGxz8QBD0eESS1f6SAzHE5zmOhtVykYth5vd6fcNcs/WVxH6By0sE8pPVpN4CSU715Clq95XsFB+jULK/9bAuulhL2lOc5zyRZ6F/ozjs7JndOgkdNcvjFeV8JbF7s7AVs+6N+rN6gy+l8OrneWZdgKYGC+FQ1Zf6+pHcO6oNrn87Uf9CfXtSMeqxmsmZZI2cMbpLiF46QfX30DB007PHiRmw9XHjX8dR90yvptDt6hwcKpiGYyp1QtccwiPLVZbjC6m8DN9PI/KdlU8TWkC2mMFlwxR18R3kHOIHbTAlwP9hxaTpnJi7PihPvL7sZU25iyUUVR5AvfbbkIhA3gxR/edvFZD36AHu+OE+AgRb82U0eRp9O+vELRiyRH7ndO4z8u82pUkLI4XIfKZrcYvyGJBnzS4JvFsNxzv3t4s7Bftro+pPhmUrfsvgM+vhZshXENqjFkYj0fRIgdy6dGmNJrq/MBbZ6tdGGS8DrPj2cP1yttFvSAWJqUI71jx2JVSubG7jzWv/i3eDHa3K8VUBp/sEudgG5KicxJTAjBgkqhkiG9w0BCRUxFgQUMIq0IcC3HvBVsZvdKBRvhmW4EUAwLTAhMAkGBSsOAwIaBQAEFEs5tRdfI99ilduwBP9b0uvUy/iyBAjqjlRGMApH+g==
ca-passphrase = 04H49FMG
skip-server-cert-verify = false